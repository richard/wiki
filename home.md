# Applications Team

## About us

Welcome to the Applications Team wiki page. The Applications Team is a group of Tor people who are working on different user facing products like [Tor Browser](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/wikis/home).

## People

The people who are part of this team is anyone working on the projects described above. We're not listing names here to keep the team open to everyone. This team also has the participation of user support and localization team members and members of other teams that give support to the Application team efforts.

You're on the team if you're participating in discussions and development, and you're not part of the team anymore if you decide you want to move on (which we hope won't happen).

## Communication

Just go to **#tor-browser-dev**, and somebody from the team might either be around or appear later and get back to you. You can use the tbb-team alias for getting the attention of someone on the team (many of us have added this specific keyword so it highlights us).

We use [​IRC](https://www.torproject.org/about/contact.html.en#irc) for our meetings, we meet on the ​[OFTC network](https://oftc.net/). We use a [meeting pad](https://pad.riseup.net/p/tor-tbb-keep) to take notes each meeting.

| Team meeting | UTC | CET |
| ------ | ------ | ------ |
| Primary team meeting | Monday 15:00 | Monday 16:00 |

## Priorities

- Improve Browser from feedback from users
- "Tor VPN" implementation
- Improve QA process for To Browser
- Sponsor work related to circumvent censorship
- Tor Browser refactoring
- start prototyping arti in android on Q2
- sustainable release process
- automated testing system in place (esp for non-linux platforms)
- ESR migration starting on time.

[Notes from session on priorities](Team-Meeting-Priorities-2023)

[Previous priorities](Previous-Priorities).

## Roadmap for Q1 - January to March 2023

### Sponsor 9
- Work on improvements coming out of usability research

### Sponsor 96
- UX have usability testing running from February–Match for desktop, so expect amends.
- Webtunnel PT happening in Q1

### Sponsor 30
- Conjure PT integration in TB

### Sponsor 131
- Release private browser in March
    - base browser + theming/branding, 
    - extensions
    - installers/packages
- Modernize testsuites for TB:
    - evaluating the test landscape, determining the tests that make sense and spending time on working on improving in this area.
    - Privacy Browser needs tests too
    - get ./mach tests working
- rlbox sandbox (the MR is ready for review already :))
- switch all the Android projects to the right SDK version, and avoid another 11.5.9
    - or, even better, avoid another crash storm like 11.5.8!
- fix the crashes with assertions on
    - last time we tried, Tor Browser was not runnable in debug mode because of some race with preferences
- start prototyping arti in android on Q2
- sustainable release process
- ESR migration starting on time.


- [Previous roadmaps](Previous-Roadmaps).

## Active Sponsor Projects

* Sponsor 9 - [Usability and Community Intervention on Support for Democracy and Human Rights](https://gitlab.torproject.org/groups/tpo/-/milestones/20)
* Sponsor 30 - [Empowering Communities in the Global South to Bypass Censorship](https://gitlab.torproject.org/tpo/anti-censorship/trac/-/issues/31265)
* Sponsor 96 - [Rapid Expansion of Access to the Uncensored Internet through Tor in China, Hong Kong, & Tibet](https://gitlab.torproject.org/groups/tpo/-/milestones/24)
* Sponsor 101 - [Tor VPN Client for Android](https://gitlab.torproject.org/groups/tpo/-/milestones/32)
* Sponsor 131 - [Refactoring of Tor Browser](https://gitlab.torproject.org/groups/tpo/applications/-/milestones/8)

## Triaging Tickets

We are triaging tickets once a week (Gaba and Richard) .

The criteria to include tickets in a specific milestone are:

- bugs that must be fixed (include security, regression or crash bugs)
- tickets that have been road-mapped before because they are part of a sponsored project
- tickets that are very fast to fix (around 1 hour)

The process we are using for triaging tickets is:

1. filter out all tickets not in the milestone sorted by creation date
2. for each new ticket:
  - add related labels (for example add an UX label to tickets that needs UX/UI help and the same for other teams)
  - if it fits the criteria written above then add it to the right milestone


## Becoming a volunteer

The best way to get involved is to visit our weekly IRC meeting (see above). Tell us your background and interests and we will find a project for your to get started.


## Archive

https://trac.torproject.org/projects/tor/wiki/org/teams/ApplicationsTeam
