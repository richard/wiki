## A Note on JDKs

we use the path the JDK path `/usr/lib/jvm/java-11-openjdk-amd64` in this doc. Use what ever is on your system, in some cases the arch is dropped and it's `/usr/lib/jvm/java-11-openjdk`. Also this has been tested with and works with `/usr/lib/jvm/java-17-openjdk-amd64` as well. For Mac (if using homebrew) its /opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/ for arm64 and /usr/local/Cellar/openjdk@11/11.0.21/libexec/openjdk.jdk/Contents/Home for x86_64.

## Prepping Tools (skip this section for mac, we get our clang via Android Studio)

Get a copy of [tor-browser-build](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser-build/) and checkout the branch that matches your build target 

*(2023.07.13: for 115.x.0-13.0 we still need [pierov:bug_40855_android](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/pierov/tor-browser-build/-/tree/bug_40855_android?ref_type=heads) please delete once merged to tbb propper)*

We need to build a clang compiler for our target, aarch64, and we need a few armv7 tools for packaging. The compiler we will use is `clang` and we also need a few extras found in `llvm-runtimes`

```
./rbm/rbm build clang --target alpha --target torbrowser-android-aarch64
./rbm/rbm build llvm-runtimes --target alpha --target torbrowser-android-aarch64
./rbm/rbm build llvm-runtimes --target alpha --target torbrowser-android-armv7
```

Then we need to assemble our compiler, but layering the llvm-runtimes on it.

```
cd out/clang
tar -xf clang-16.x.y-hhhhhh.tar.gz
mv clang clang-16.x.y-arm
tar -xzf ./../llvm-runtimes/llvm-runtimes-16.x.y-aarch64-hhhhhh.tar.gz
tar -xzf ./../llvm-runtimes/llvm-runtimes-16.x.y-armv7-hhhhhh.tar.gz
cp -r llvm-runtimes/lib/clang/16/lib/ clang-16.x.y-arm/lib/clang/16/
```

And now both arm7 and aarch64 llvm-runtimes are installed in your clang and it's ready for use.

## Building Geckoview

Geckoview is the core browser engine. It will be compiled for android and prepared for packaging in an android browser.

clone a copy of [tor-browser](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser) and check out the relevant branch (eg: tor-browser-115.2.1-13.5-1)

create an `android-env.sh` and fill it accordingly to your setup with the following


For Linux:
```
export MOZ_BUILD_DATE=20230710165010 # This should match the data in [firefox-android](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/firefox-android)/android-components/plugins/dependencies/src/main/java/Gecko.kt ~ln 12's def of the variable *version*, the date component
export JAVA_HOME=/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/ # for arm64. Or JAVA_HOME=/usr/local/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/
export ANDROID_HOME=$HOME/.mozbuild/android-sdk-linux/ # or $HOME/Android/Sdk/ # Or .../android-toolchain/android-sdk-linux if you extract android-toolchain from tor-browser-build
export ANDROID_NDK_HOME=$ANDROID_HOME/ndk/23.2.8568313/ # for 115esr
export GRADLE_HOME=/FULL/PATH/TO/tor-browser-build/out/gradle/gradle-7.5.1 # Or the version that we currently use
export LOCAL_DEV_BUILD=1
export PATH=/FULL/PATH/TO/tor-browser-build/out/clang/clang-16.x.y-arm/bin/:$PATH # prepend our newly built and assembled clang to the path so it gets used to build geckoview
```

For Mac: (default is arm64, note the different paths for x86_64). Make sure to download the appropriate NDK's via Android Studio's SDK manager (you can select the specific version by checking the box in the bottom right that says "Show Package Details") ![Screenshot_2023-11-07_at_14.04.00](uploads/bb5eaa40b39284829fb3f21377c61b6f/Screenshot_2023-11-07_at_14.04.00.png)
```
export MOZ_BUILD_DATE=20230710165010 # This should match the data in [firefox-android](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/firefox-android)/android-components/plugins/dependencies/src/main/java/Gecko.kt ~ln 12's def of the variable *version*, the date component
export JAVA_HOME=/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/ # for arm64. Or JAVA_HOME=/usr/local/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/
export ANDROID_HOME=$HOME/Library/Android/sdk # or $HOME/.mozbuild/android-sdk-macosx/
export ANDROID_NDK_HOME=$ANDROID_HOME/ndk/23.2.8568313 # will need to download NDK 23.2.8568313 via android studio
export GRADLE_HOME=/opt/homebrew/Cellar/gradle@7/7.6.3 # for arm64 or /usr/local/Cellar/gradle@7/7.6.3 for x86_64. Make sure the version is up to date
export LOCAL_DEV_BUILD=1
export PATH=$ANDROID_HOME/ndk/25.2.9519653/toolchains/llvm/prebuilt/darwin-x86_64/bin/:$PATH # prepend android studios latest ndk to the path so it's clang gets used to build geckoview. NDK 25.2.9519653 uses clang 14.0.7, ideally we'd use clang 16 (to be the same as Linux) but that's not an option yet for android studio. NDK 26.1.10909125 uses clang 17.0.2, which we should evaluate with the esr128 migration
```

Make sure that the variables defined are all valid for your setup, and that PATH is now using our newly built clang.

Then `source android-env.sh` to include

<details>
<summary>You may want to run `./mach bootstrap` the first time to get some deps</summary>

Select "3", installing deps as recommended. This will also populate some ~/.mozbuild files and downloads. DO NOT let it setup your git, it can just as easily accidentally break it, so select 'n'. Likewise it's probably best to select 'n' when asked about submitting code to Mozilla. We currently maintain our own configs so also select 'n' for overwriting mozconfigs.
</details>

To build:

```
MOZCONFIG=mozconfig-android-aarch64 ./mach clobber
MOZCONFIG=mozconfig-android-aarch64  ./mach configure --without-wasm-sandboxed-libraries
```

<details>
<summary>You may need to use `rustup` to install and select an appropriate rust version and target.</summary>

```
rustup default 1.69
rustup target add aarch64-linux-android
```
</details>

then build

```
MOZCONFIG=mozconfig-android-aarch64 ./mach build
```

### Packaging and publishing Geckoview for use in Android

First we make a "fat aar"

```
MOZCONFIG=mozconfig-android-all ./mach clobber
cd tools/torbrowser
env ARCHS=aarch64 make fat-aar
cd ../..
```

and then we publish to our local gradle repository

```
MOZCONFIG=mozconfig-android-aarch64 ./mach build binaries
MOZCONFIG=mozconfig-android-aarch64 ./mach gradle geckoview:publishWithGeckoBinariesDebugPublicationToMavenRepository

MOZCONFIG=mozconfig-android-all ./mach gradle geckoview:publishWithGeckoBinariesDebugPublicationToMavenLocal exoplayer2:publishDebugPublicationToMavenLocal
```

and after all this we should be able to confirm by running  `ls ~/.m2/repository/org/mozilla/geckoview/geckoview-default-omni` and seeing our new files

## Building firefox-android

Get a copy of the repo [firefox-android](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/firefox-android) and check out the relevant build branch (eg: firefox-android-115.2.0-13.0-1)

### Android Components

edit `android-components/plugins/dependencies/src/main/java/Gecko.kt` and change:
- `const val version = "115.0.20230710165010"` to have the correct version that matches what the tor-browser/geckoview version, for instance you might have to change the version to "115.2". The date part should have been previously copied from here to android-env.sh in tor-browser in the previous step. You can confirm you have it right with `ls ~/.m2/repository/org/mozilla/geckoview/geckoview-default-omni` which will show you what geckoview published. You need to match your target
- `NIGHTLY("geckoview-nightly-omni"),` to `NIGHTLY("geckoview-default-omni"),`
- `val channel = GeckoChannel.RELEASE`  to `val channel = GeckoChannel.NIGHTLY` if it's not like that already.

then open `android-components/local.properties` and populate it with the following

```
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.

sdk.dir=/home/YOU/Android/Sdk
dependencySubstitutions.geckoviewTopobjdir=/FULL/PATH/TO/tor-browser/obj-arm-unknown-linux-androideabi
dependencySubstitutions.geckoviewTopsrcdir=/FULL/PATH/TO/tor-browser
```

remeber to set the variables to your local setup

then open `android-components` in android studio and build it (`Build > Make Project`)

### Fenix

Edit `fenix/gradle.properties` and add `org.gradle.java.home=/usr/lib/jvm/java-11-openjdk-amd64/` to the bottom (or the proper path for your system).

Then open `fenix/local.properties` and fill as follows

```
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.
#Tue Jun 27 16:08:20 PDT 2023

sdk.dir=/home/YOU/Android/Sdk
dependencySubstitutions.geckoviewTopobjdir=/FULL/PATH/TO/tor-browser/obj-arm-unknown-linux-androideabi
dependencySubstitutions.geckoviewTopsrcdir=/FULL/PATH/TO/tor-browser
autoPublish.android-components.dir=../android-components
```

You will also need the artifacts from `tor-android-service` and `tor-onion-proxy-library`.
If ever ran an Android build with tor-browser-build, you can use the `fenix/tools/tba-fetch-deps.sh` script.
It assumes `tor-browser-build.git` has been checked out at the same level of `firefox-android.git`, but you can also specify another directory with the `TOR_BROWSER_BUILD` environment variable.

Open `fenix` in android studio and build (`Build > Make Project`).

As an alternative, you can also build from the command line:

```
gradle --no-daemon -Dorg.gradle.jvmargs=-Xmx20g assemble$variant
```

where `$variant` must be one of `Release`, `Beta` or `Nightly` (notice the capital letter to make the result camelCase).

## Running

From Android Studio with `fenix` loaded simply select from the menu `Run > Run 'app'` with an appropriate android device with USB debugging enabled plugged in.

Otherwise, you can sign the apks with the same QA key we use for the builds from tor-browser-build with `fenix/tools/tba-sign-devbuilds.sh`.
This binary assumes that you have `tor-browser-build.git` at the same level of `firefox-android.git`, but you can override this with the `TOR_BROWSER_BUILD` environment variable.