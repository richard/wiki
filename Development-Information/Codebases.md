Like many large software projects, Tor Browser integrates many distinct codebases.

# Tor Browser

[Repository](https://gitlab.torproject.org/tpo/applications/tor-browser)

Tor Browser is based on Firefox, and this repository contains our fork of Mozilla's [mozilla-central](https://hg.mozilla.org/mozilla-central/).
From this code, we build both Tor Browser for desktop (TBB, Tor Browser Bundle) and [GeckoView](https://mozilla.github.io/geckoview/), which is the rendering engine for Tor Browser for Android (TBA).

Every time a new Firefox version is released, we create four new branches on this project, then we reapply our patch set on top of it.

Therefore, the repository does not have a mainline, and we work concurrently on four branches:

1. Tor Browser stable for desktop
2. Tor Browser alpha for desktop
3. Geckoview stable
4. Geckoview alpha

While Mozilla keeps the codebase for Firefox and GeckoView united, we keep it separate for some reasons:

* desktop is based on the ESR channel, Android on the rapid release one;
* while many patches are the same, others are incompatible between the two platforms or are implemented differently because of architectural differences.

# Firefox-android

[Repository](https://gitlab.torproject.org/tpo/applications/firefox-android)

Firefox-android is the new mozilla merged codebase of old Fenix and Android Components. It is all the code needed to take a geckoview build from tor-browser and produce and android browser.

- [Building instructions](Development-Information/Firefox-Android/Building)

<details>
<summary>Legacy Fenix and Android Components projects</summary>

# Fenix

[Repository](https://gitlab.torproject.org/tpo/applications/fenix)

Fenix is the codename of Firefox for Android. This repository contains the code to build Tor Browser Android from the patched GeckoView and Android Components.

Like other repositories, it does not have a mainline. Each branch is a version released by Mozilla, with our patch set applied.

# Android Components

[Repository](https://gitlab.torproject.org/tpo/applications/android-components)

[Mozilla Android Components](https://mozac.org/) is a collection of independent, reusable Android library components to make it easier to build browsers and browser-like applications.

It is another of the pieces used to build TBA.

Like Fenix and GeckoView, each branch is a MozAC version, plus Tor Browser patches.

</details>

# Tor Android Service

[Repository](https://gitweb.torproject.org/tor-android-service.git/)

It is the Tor controller for Android.

# Tor Browser Build

[Repository](https://gitlab.torproject.org/tpo/applications/tor-browser-build)

It is the repository we use to create reproducible builds of Tor Browser for desktop and Android and manage all their dependencies.

It is based on [RBM (Reproducible Build System)](https://rbm.torproject.org/).

# Tor Browser Bundle Testsuite

[Repository](https://gitlab.torproject.org/tpo/applications/tor-browser-bundle-testsuite)

It contains a test suite for Tor Browser.